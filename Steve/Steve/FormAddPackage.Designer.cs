﻿namespace Steve
{
    partial class FormAddPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_nrPackage = new System.Windows.Forms.TextBox();
            this.text_dataDelivery = new System.Windows.Forms.TextBox();
            this.text_idUser = new System.Windows.Forms.TextBox();
            this.chosen_status = new System.Windows.Forms.ComboBox();
            this.zatwierdz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // text_nrPackage
            // 
            this.text_nrPackage.Location = new System.Drawing.Point(73, 48);
            this.text_nrPackage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_nrPackage.Name = "text_nrPackage";
            this.text_nrPackage.Size = new System.Drawing.Size(177, 22);
            this.text_nrPackage.TabIndex = 0;
            this.text_nrPackage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_dataDelivery
            // 
            this.text_dataDelivery.Location = new System.Drawing.Point(73, 91);
            this.text_dataDelivery.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_dataDelivery.Name = "text_dataDelivery";
            this.text_dataDelivery.Size = new System.Drawing.Size(177, 22);
            this.text_dataDelivery.TabIndex = 1;
            this.text_dataDelivery.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_idUser
            // 
            this.text_idUser.Location = new System.Drawing.Point(73, 134);
            this.text_idUser.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_idUser.Name = "text_idUser";
            this.text_idUser.Size = new System.Drawing.Size(177, 22);
            this.text_idUser.TabIndex = 2;
            this.text_idUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chosen_status
            // 
            this.chosen_status.FormattingEnabled = true;
            this.chosen_status.Items.AddRange(new object[] {
            "dostarczono",
            "w drodze",
            "w magazynie",
            "w systemie"});
            this.chosen_status.Location = new System.Drawing.Point(73, 170);
            this.chosen_status.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chosen_status.Name = "chosen_status";
            this.chosen_status.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chosen_status.Size = new System.Drawing.Size(177, 24);
            this.chosen_status.TabIndex = 4;
            this.chosen_status.Text = "status";
            // 
            // zatwierdz
            // 
            this.zatwierdz.Location = new System.Drawing.Point(73, 226);
            this.zatwierdz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.zatwierdz.Name = "zatwierdz";
            this.zatwierdz.Size = new System.Drawing.Size(177, 43);
            this.zatwierdz.TabIndex = 5;
            this.zatwierdz.Text = "Zadwierdz";
            this.zatwierdz.UseVisualStyleBackColor = true;
            this.zatwierdz.Click += new System.EventHandler(this.zatwierdz_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nr. paczki";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Data dostawy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 115);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Id odbiorcy";
            // 
            // FormAddPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 358);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zatwierdz);
            this.Controls.Add(this.chosen_status);
            this.Controls.Add(this.text_idUser);
            this.Controls.Add(this.text_dataDelivery);
            this.Controls.Add(this.text_nrPackage);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormAddPackage";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Dodaj paczke";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_nrPackage;
        private System.Windows.Forms.TextBox text_dataDelivery;
        private System.Windows.Forms.TextBox text_idUser;
        private System.Windows.Forms.ComboBox chosen_status;
        private System.Windows.Forms.Button zatwierdz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}