﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Steve
{
    public partial class FormPackage : Form
    {
        readonly FormAddPackage _addPackage = new FormAddPackage();
        public FormPackage()
        {
            _addPackage.AddedNewRecord += OnAddedNewRecord;
            InitializeComponent();
        }

        protected override void OnActivated(EventArgs e)
        {
            ChangeList();
        }



        public void OnAddedNewRecord(object o, EventArgs e)
        {
            ChangeList();
        }



        public void ChangeList()
        {
            var listp = new List<Package>();
            var xmlser = new XmlSerializer(typeof(List<Package>));
            FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Package.xml", FileMode.Open, FileAccess.Read);
            listp = (List<Package>)xmlser.Deserialize(filestrem);

            filestrem.Close();
            listView_package.Items.Clear();
            foreach (var kk in listp)
            {

                var item = new ListViewItem(kk.Id);
                item.SubItems.Add(kk.Status);
                item.SubItems.Add(kk.Data);
                item.SubItems.Add(kk.IdUser);
                listView_package.Items.Add(item);
            }
        }


        private void btn_dodaj_Click(object sender, EventArgs e)
        {
            _addPackage.ShowDialog();
        }

        private void button_changeStatus_Click(object sender, EventArgs e)
        {
            if (listView_package.SelectedItems.Count > 0)
            {
                var item = listView_package.SelectedItems[0];
                
                XmlSerializer _xmlser = new XmlSerializer(typeof(List<Package>));
                List<Package> _listp = new List<Package>();
                FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Package.xml", FileMode.Open, FileAccess.Read);
                _listp = (List<Package>)_xmlser.Deserialize(filestrem);
                filestrem.Close();
               
                int index = _listp.FindIndex(r => r.Id == item.SubItems[0].Text);
                _listp[index].Status = chosen_status.Text;
                
                filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Package.xml", FileMode.Create, FileAccess.Write);
                _xmlser.Serialize(filestrem, _listp);
                filestrem.Close();
                ChangeList();
                    
            }
        }
    }
}
