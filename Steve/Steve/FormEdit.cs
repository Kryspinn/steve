﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Steve
{
    public partial class FormEdit : Form
    {
        public delegate void AddedNewRecordEventHandler(object o, EventArgs e);

        public event AddedNewRecordEventHandler AddedNewRecord;

        protected virtual void OnAddedNewRecord()
        {
            AddedNewRecord?.Invoke(this, EventArgs.Empty);
        }
        public FormEdit()
        {
            InitializeComponent();
            _listk = new List<Klient>();
            _xmlser = new XmlSerializer(typeof(List<Klient>));
        }


        private readonly XmlSerializer _xmlser;
        List<Klient> _listk;
        private Klient itemToChange = new Klient();
        private int index;
        private bool finded = false;
        

        private void button_find_Click(object sender, EventArgs e)
        {
            FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Open, FileAccess.Read);
            _listk = (List<Klient>)_xmlser.Deserialize(filestrem);
            filestrem.Close();
            try
            {
                itemToChange = _listk.Single(r => r.Name == text_name.Text);
                finded = true;
                
            }
            catch (Exception exception)
            {
                MessageBox.Show("Nie znaleziono rekordu lub jest ich wiele. Szczegol: \n" + exception);
                finded = false;
                //throw;
            }

            if (finded)
            {
                index = _listk.FindIndex(r => r.Name == text_name.Text);
                text_newname.Text = _listk[index].Name;
                text_newsurname.Text = _listk[index].Surname;
                text_newadress.Text = _listk[index].Adress;
                MessageBox.Show("Znaleziono rekord");
            }
            
        }

        private void button_edit_Click(object sender, EventArgs e)
        {
            if (finded)
            {
                _listk[index].Name = text_newname.Text;
                _listk[index].Surname = text_newsurname.Text;
                _listk[index].Adress = text_newadress.Text;
                FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Create, FileAccess.Write);
                _xmlser.Serialize(filestrem, _listk);
                filestrem.Close();
                OnAddedNewRecord();
            }
            else MessageBox.Show("Najpierw wyszukaj rekodu");
        }
    }
}
