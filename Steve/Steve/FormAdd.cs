﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Threading;


namespace Steve
{
    public partial class FormAdd : Form
    {
        public delegate void AddedNewRecordEventHandler(object o, EventArgs e);

        public event AddedNewRecordEventHandler AddedNewRecord;

        protected virtual void OnAddedNewRecord()
        {
            AddedNewRecord?.Invoke(this, EventArgs.Empty);
        }

        XmlSerializer xmlser;
        List<Klient> listk;
        public FormAdd()
        {
            InitializeComponent();
            listk = new List<Klient>();
            xmlser = new XmlSerializer(typeof(List<Klient>));

        }

        private void zatwierdz_Click(object sender, EventArgs e)
        {
            bool allOK = true;


            //if(text_name.Text == null && text_name.Text.Length <= 3)
            //{
            //    allOK = false;
            //    MessageBox.Show("Zła wartość w kolumnie IMIE");
            //    text_name.Text = string.Empty;
            //    text_name.Focus();
               
            //}
            //if (text_surname.Text == null && text_surname.Text.Length <= 3)
            //{
            //    allOK = false;
            //    MessageBox.Show("Zła wartość w kolumnie Nazwisko");
            //    text_surname.Text = string.Empty;
            //    text_surname.Focus();
            //}
            //if (text_adress.Text == null && text_adress.Text.Length <= 3)
            //{
            //    allOK = false;
            //    MessageBox.Show("Zła wartość w kolumnie Adres");
            //    text_adress.Text = string.Empty;
            //    text_adress.Focus();
            //}


            if(allOK == true)
            {

                FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Open, FileAccess.Read);
                listk = (List<Klient>)xmlser.Deserialize(filestrem);
                filestrem.Close();
                
                Klient k1 = new Klient();



                k1.Id = listk[listk.Count - 1].Id + 1;

                for (int i = 0; i < listk.Count; i++)
                {
                    if (listk[i].Id == k1.Id)
                    {
                        k1.Id++;
                        i = -1;
                    }
                }

                k1.Name = text_name.Text;
                k1.Surname = text_surname.Text;
                k1.Adress = text_adress.Text;                    
                listk.Add(k1);

                filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Create, FileAccess.Write);
                xmlser.Serialize(filestrem, listk);
                filestrem.Close();
                OnAddedNewRecord();

                //foreach (Klient kk in listk)
                //{
                //    MessageBox.Show("Zła wartość w kolumnie Adres");
                //    MessageBox.Show(kk.Name);
                //    ListViewItem item = new ListViewItem(kk.Name);
                //    item.SubItems.Add(kk.Surname);
                //    item.SubItems.Add(kk.Adress);
                //    f.listView_klient.Items.Add(item);
                //}


                
            }

        }


        
    }
}
