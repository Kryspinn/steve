﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Steve
{

    public partial class FormDelete : Form
    {

        public delegate void AddedNewRecordEventHandler(object o, EventArgs e);

        public event AddedNewRecordEventHandler AddedNewRecord;

        protected virtual void OnAddedNewRecord()
        {
            AddedNewRecord?.Invoke(this, EventArgs.Empty);
        }




        private readonly XmlSerializer _xmlser;
        List<Klient> _listk;
        private bool _found;
        Klient itemToRemove = new Klient();
        public FormDelete()
        {
            InitializeComponent();
            _listk = new List<Klient>();
            _xmlser = new XmlSerializer(typeof(List<Klient>));
        }

        public void Delete_Click(object sender, EventArgs e)
        {
            FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Open, FileAccess.Read);
            _listk = (List<Klient>)_xmlser.Deserialize(filestrem);
            filestrem.Close();

            
            try
            {
                itemToRemove = _listk.Single(r => r.Name == text_name.Text); // more conditions
                _found = true;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Nie znaleziono rekordu lub jest ich wiele. Szczegol: \n" + exception);
                _found = false;
                //throw;
            }

            if (_found)
            {
                _listk.Remove(itemToRemove);
                filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Create, FileAccess.Write);
                _xmlser.Serialize(filestrem, _listk);
                filestrem.Close();
                OnAddedNewRecord();
            }

        }
    }
}
