﻿namespace Steve
{
    partial class FormDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.text_surname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.text_adress = new System.Windows.Forms.TextBox();
            this.zatwierdz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // text_name
            // 
            this.text_name.Location = new System.Drawing.Point(54, 47);
            this.text_name.Margin = new System.Windows.Forms.Padding(2);
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(134, 20);
            this.text_name.TabIndex = 1;
            this.text_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Imię";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Nazwisko";
            // 
            // text_surname
            // 
            this.text_surname.Location = new System.Drawing.Point(54, 84);
            this.text_surname.Margin = new System.Windows.Forms.Padding(2);
            this.text_surname.Name = "text_surname";
            this.text_surname.Size = new System.Drawing.Size(134, 20);
            this.text_surname.TabIndex = 9;
            this.text_surname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Adres";
            // 
            // text_adress
            // 
            this.text_adress.Location = new System.Drawing.Point(54, 121);
            this.text_adress.Margin = new System.Windows.Forms.Padding(2);
            this.text_adress.Name = "text_adress";
            this.text_adress.Size = new System.Drawing.Size(134, 20);
            this.text_adress.TabIndex = 11;
            this.text_adress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // zatwierdz
            // 
            this.zatwierdz.Location = new System.Drawing.Point(54, 230);
            this.zatwierdz.Margin = new System.Windows.Forms.Padding(2);
            this.zatwierdz.Name = "zatwierdz";
            this.zatwierdz.Size = new System.Drawing.Size(133, 35);
            this.zatwierdz.TabIndex = 15;
            this.zatwierdz.Text = "Usun";
            this.zatwierdz.UseVisualStyleBackColor = true;
            this.zatwierdz.Click += new System.EventHandler(this.Delete_Click);
            // 
            // FormDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(258, 327);
            this.Controls.Add(this.zatwierdz);
            this.Controls.Add(this.text_adress);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.text_surname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_name);
            this.Name = "FormDelete";
            this.Text = "FormDelete";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox text_surname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox text_adress;
        private System.Windows.Forms.Button zatwierdz;
    }
}