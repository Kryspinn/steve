﻿namespace Steve
{
    partial class FormEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_name = new System.Windows.Forms.TextBox();
            this.text_surname = new System.Windows.Forms.TextBox();
            this.text_adress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_find = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.text_newname = new System.Windows.Forms.TextBox();
            this.text_newsurname = new System.Windows.Forms.TextBox();
            this.text_newadress = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button_edit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // text_name
            // 
            this.text_name.Location = new System.Drawing.Point(48, 54);
            this.text_name.Margin = new System.Windows.Forms.Padding(2);
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(134, 20);
            this.text_name.TabIndex = 2;
            this.text_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_surname
            // 
            this.text_surname.Location = new System.Drawing.Point(48, 94);
            this.text_surname.Margin = new System.Windows.Forms.Padding(2);
            this.text_surname.Name = "text_surname";
            this.text_surname.Size = new System.Drawing.Size(134, 20);
            this.text_surname.TabIndex = 10;
            this.text_surname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_adress
            // 
            this.text_adress.Location = new System.Drawing.Point(48, 133);
            this.text_adress.Margin = new System.Windows.Forms.Padding(2);
            this.text_adress.Name = "text_adress";
            this.text_adress.Size = new System.Drawing.Size(134, 20);
            this.text_adress.TabIndex = 12;
            this.text_adress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Imię";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Nazwisko";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Adres";
            // 
            // button_find
            // 
            this.button_find.Location = new System.Drawing.Point(49, 178);
            this.button_find.Margin = new System.Windows.Forms.Padding(2);
            this.button_find.Name = "button_find";
            this.button_find.Size = new System.Drawing.Size(133, 35);
            this.button_find.TabIndex = 16;
            this.button_find.Text = "Znajdz";
            this.button_find.UseVisualStyleBackColor = true;
            this.button_find.Click += new System.EventHandler(this.button_find_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(49, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Znajdz rekord";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(237, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Nowe dane";
            // 
            // text_newname
            // 
            this.text_newname.Location = new System.Drawing.Point(240, 54);
            this.text_newname.Margin = new System.Windows.Forms.Padding(2);
            this.text_newname.Name = "text_newname";
            this.text_newname.Size = new System.Drawing.Size(134, 20);
            this.text_newname.TabIndex = 19;
            this.text_newname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_newsurname
            // 
            this.text_newsurname.Location = new System.Drawing.Point(240, 94);
            this.text_newsurname.Margin = new System.Windows.Forms.Padding(2);
            this.text_newsurname.Name = "text_newsurname";
            this.text_newsurname.Size = new System.Drawing.Size(134, 20);
            this.text_newsurname.TabIndex = 20;
            this.text_newsurname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_newadress
            // 
            this.text_newadress.Location = new System.Drawing.Point(240, 133);
            this.text_newadress.Margin = new System.Windows.Forms.Padding(2);
            this.text_newadress.Name = "text_newadress";
            this.text_newadress.Size = new System.Drawing.Size(134, 20);
            this.text_newadress.TabIndex = 21;
            this.text_newadress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(237, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 22;
            this.label6.Text = "Adres";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(237, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Nazwisko";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(237, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "Imię";
            // 
            // button_edit
            // 
            this.button_edit.Location = new System.Drawing.Point(241, 178);
            this.button_edit.Margin = new System.Windows.Forms.Padding(2);
            this.button_edit.Name = "button_edit";
            this.button_edit.Size = new System.Drawing.Size(133, 35);
            this.button_edit.TabIndex = 25;
            this.button_edit.Text = "Zmien";
            this.button_edit.UseVisualStyleBackColor = true;
            this.button_edit.Click += new System.EventHandler(this.button_edit_Click);
            // 
            // FormEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 268);
            this.Controls.Add(this.button_edit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.text_newadress);
            this.Controls.Add(this.text_newsurname);
            this.Controls.Add(this.text_newname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_find);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_adress);
            this.Controls.Add(this.text_surname);
            this.Controls.Add(this.text_name);
            this.Name = "FormEdit";
            this.Text = "FormEdit";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.TextBox text_surname;
        private System.Windows.Forms.TextBox text_adress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_find;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox text_newname;
        private System.Windows.Forms.TextBox text_newsurname;
        private System.Windows.Forms.TextBox text_newadress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_edit;
    }
}