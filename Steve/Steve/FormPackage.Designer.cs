﻿namespace Steve
{
    partial class FormPackage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_dodaj = new System.Windows.Forms.Button();
            this.button_changeStatus = new System.Windows.Forms.Button();
            this.listView_package = new System.Windows.Forms.ListView();
            this.Nrpackage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.IdUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.chosen_status = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btn_dodaj
            // 
            this.btn_dodaj.Location = new System.Drawing.Point(36, 54);
            this.btn_dodaj.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_dodaj.Name = "btn_dodaj";
            this.btn_dodaj.Size = new System.Drawing.Size(110, 53);
            this.btn_dodaj.TabIndex = 1;
            this.btn_dodaj.Text = "Dodaj";
            this.btn_dodaj.UseVisualStyleBackColor = true;
            this.btn_dodaj.Click += new System.EventHandler(this.btn_dodaj_Click);
            // 
            // button_changeStatus
            // 
            this.button_changeStatus.Location = new System.Drawing.Point(412, 54);
            this.button_changeStatus.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_changeStatus.Name = "button_changeStatus";
            this.button_changeStatus.Size = new System.Drawing.Size(110, 53);
            this.button_changeStatus.TabIndex = 4;
            this.button_changeStatus.Text = "Zmień status";
            this.button_changeStatus.UseVisualStyleBackColor = true;
            this.button_changeStatus.Click += new System.EventHandler(this.button_changeStatus_Click);
            // 
            // listView_package
            // 
            this.listView_package.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nrpackage,
            this.Status,
            this.Data,
            this.IdUser});
            this.listView_package.FullRowSelect = true;
            this.listView_package.GridLines = true;
            this.listView_package.Location = new System.Drawing.Point(37, 113);
            this.listView_package.Margin = new System.Windows.Forms.Padding(4);
            this.listView_package.Name = "listView_package";
            this.listView_package.Size = new System.Drawing.Size(507, 258);
            this.listView_package.TabIndex = 7;
            this.listView_package.UseCompatibleStateImageBehavior = false;
            this.listView_package.View = System.Windows.Forms.View.Details;
            // 
            // Nrpackage
            // 
            this.Nrpackage.Text = "Nr paczki";
            this.Nrpackage.Width = 88;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            this.Status.Width = 97;
            // 
            // Data
            // 
            this.Data.Text = "Data Dostawy";
            this.Data.Width = 147;
            // 
            // IdUser
            // 
            this.IdUser.Text = "Id Odbiiorcy";
            this.IdUser.Width = 162;
            // 
            // chosen_status
            // 
            this.chosen_status.FormattingEnabled = true;
            this.chosen_status.Items.AddRange(new object[] {
            "dostarczono",
            "w drodze",
            "w magazynie",
            "w systemie"});
            this.chosen_status.Location = new System.Drawing.Point(214, 69);
            this.chosen_status.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chosen_status.Name = "chosen_status";
            this.chosen_status.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chosen_status.Size = new System.Drawing.Size(177, 24);
            this.chosen_status.TabIndex = 8;
            this.chosen_status.Text = "status";
            // 
            // FormPackage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 403);
            this.Controls.Add(this.chosen_status);
            this.Controls.Add(this.listView_package);
            this.Controls.Add(this.button_changeStatus);
            this.Controls.Add(this.btn_dodaj);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormPackage";
            this.Text = "FormPackage";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_dodaj;
        private System.Windows.Forms.Button button_changeStatus;
        public System.Windows.Forms.ListView listView_package;
        public System.Windows.Forms.ColumnHeader Nrpackage;
        public System.Windows.Forms.ColumnHeader Status;
        public System.Windows.Forms.ColumnHeader Data;
        private System.Windows.Forms.ColumnHeader IdUser;
        private System.Windows.Forms.ComboBox chosen_status;
    }
}