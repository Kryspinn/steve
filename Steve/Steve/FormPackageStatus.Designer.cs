﻿namespace Steve
{
    partial class FormPackageStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView_package = new System.Windows.Forms.ListView();
            this.Nrpackage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Data = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.text_surname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_find = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listView_package
            // 
            this.listView_package.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Nrpackage,
            this.Status,
            this.Data});
            this.listView_package.FullRowSelect = true;
            this.listView_package.GridLines = true;
            this.listView_package.Location = new System.Drawing.Point(62, 130);
            this.listView_package.Margin = new System.Windows.Forms.Padding(4);
            this.listView_package.Name = "listView_package";
            this.listView_package.Size = new System.Drawing.Size(352, 287);
            this.listView_package.TabIndex = 8;
            this.listView_package.UseCompatibleStateImageBehavior = false;
            this.listView_package.View = System.Windows.Forms.View.Details;
            // 
            // Nrpackage
            // 
            this.Nrpackage.Text = "Nr paczki";
            this.Nrpackage.Width = 88;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            this.Status.Width = 97;
            // 
            // Data
            // 
            this.Data.Text = "Data Dostawy";
            this.Data.Width = 147;
            // 
            // text_surname
            // 
            this.text_surname.Location = new System.Drawing.Point(62, 76);
            this.text_surname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_surname.Name = "text_surname";
            this.text_surname.Size = new System.Drawing.Size(177, 22);
            this.text_surname.TabIndex = 9;
            this.text_surname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 46);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Podaj nazwisko";
            // 
            // button_find
            // 
            this.button_find.Location = new System.Drawing.Point(271, 36);
            this.button_find.Name = "button_find";
            this.button_find.Size = new System.Drawing.Size(133, 62);
            this.button_find.TabIndex = 11;
            this.button_find.Text = "Zanajdz";
            this.button_find.UseVisualStyleBackColor = true;
            this.button_find.Click += new System.EventHandler(this.button_find_Click);
            // 
            // FormPackageStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 464);
            this.Controls.Add(this.button_find);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.text_surname);
            this.Controls.Add(this.listView_package);
            this.Name = "FormPackageStatus";
            this.Text = "FormPackageStatus";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListView listView_package;
        public System.Windows.Forms.ColumnHeader Nrpackage;
        public System.Windows.Forms.ColumnHeader Status;
        public System.Windows.Forms.ColumnHeader Data;
        private System.Windows.Forms.TextBox text_surname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_find;
    }
}