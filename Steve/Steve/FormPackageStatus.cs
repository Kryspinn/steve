﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Steve
{
    public partial class FormPackageStatus : Form
    {
        private readonly XmlSerializer _xmlserk;
        List<Klient> _listk;
        private readonly XmlSerializer _xmlserp;
        List<Package> _listp;
        private bool finded = false;

        public FormPackageStatus()
        {
            InitializeComponent();
            _listk = new List<Klient>();
            _xmlserk = new XmlSerializer(typeof(List<Klient>));

            _listp = new List<Package>();
            _xmlserp = new XmlSerializer(typeof(List<Package>));
        }

        private void button_find_Click(object sender, EventArgs e)
        {
            FileStream filestremk = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Open, FileAccess.Read);
            _listk = (List<Klient>)_xmlserk.Deserialize(filestremk);
            filestremk.Close();
            FileStream filestremp = new FileStream("E:\\KRD\\Steve\\Steve\\Package.xml", FileMode.Open, FileAccess.Read);
            _listp = (List<Package>)_xmlserp.Deserialize(filestremp);
            filestremp.Close();
            Klient item = new Klient();
            try
            {
                item = _listk.Single(r => r.Surname == text_surname.Text);
                
                finded = true;

            }
            catch (Exception exception)
            {
                MessageBox.Show("Nie znaleziono rekordu lub jest ich wiele. Szczegol: \n" + exception);
                finded = false;
                //throw;
            }

            if (finded)
            {
                
                List<Package> _listPackagesUser = new List<Package>();
                _listPackagesUser = _listp.FindAll(r => r.IdUser == item.Id.ToString());
                foreach (var kk in _listPackagesUser)
                {

                    var item1 = new ListViewItem(kk.Id);
                    item1.SubItems.Add(kk.Status);
                    item1.SubItems.Add(kk.Data);
                    
                    listView_package.Items.Add(item1);
                }
            }

        }
    }
    
}
