﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Threading;


namespace Steve
{
    public partial class FormAddPackage: Form
    {
        public delegate void AddedNewRecordEventHandler(object o, EventArgs e);

        public event AddedNewRecordEventHandler AddedNewRecord;

        protected virtual void OnAddedNewRecord()
        {
            AddedNewRecord?.Invoke(this, EventArgs.Empty);
        }

        XmlSerializer xmlser;
        List<Package> _listp;
        public FormAddPackage()
        {
            InitializeComponent();
            _listp = new List<Package>();
            xmlser = new XmlSerializer(typeof(List<Package>));

        }

        private void zatwierdz_Click(object sender, EventArgs e)
        {
            bool allOK = true;


            //if (text_name.Text == null && text_name.Text.Length <= 3)
            //{
            //    allOK = false;
            //    MessageBox.Show("Zła wartość w kolumnie IMIE");
            //    text_name.Text = string.Empty;
            //    text_name.Focus();

            //}
            //if (text_surname.Text == null && text_surname.Text.Length <= 3)
            //{
            //    allOK = false;
            //    MessageBox.Show("Zła wartość w kolumnie Nazwisko");
            //    text_surname.Text = string.Empty;
            //    text_surname.Focus();
            //}
            //if (text_adress.Text == null && text_adress.Text.Length <= 3)
            //{
            //    allOK = false;
            //    MessageBox.Show("Zła wartość w kolumnie Adres");
            //    text_adress.Text = string.Empty;
            //    text_adress.Focus();
            //}


            if (allOK == true)
            {

                FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Package.xml", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                _listp = (List<Package>)xmlser.Deserialize(filestrem);
                filestrem.Close();


                Package p1 = new Package
                {
                    Id = text_nrPackage.Text,
                    Data = text_dataDelivery.Text,
                    IdUser = text_idUser.Text,
                    Status = chosen_status.Text
                };



                _listp.Add(p1);

                filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Package.xml", FileMode.Create, FileAccess.Write);
                xmlser.Serialize(filestrem, _listp);
                filestrem.Close();
                OnAddedNewRecord();

                //foreach (Package kk in listp)
                //{
                //    MessageBox.Show("Zła wartość w kolumnie Adres");
                //    MessageBox.Show(kk.Name);
                //    ListViewItem item = new ListViewItem(kk.Name);
                //    item.SubItems.Add(kk.Surname);
                //    item.SubItems.Add(kk.Adress);
                //    f.listView_Package.Items.Add(item);
                //}



            }

        }


    }
}
