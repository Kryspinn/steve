﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Steve
{
    public class Package
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string Data { get; set; }
        public string IdUser { get; set; }
    }
}
