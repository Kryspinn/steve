﻿namespace Steve
{
    partial class FormChose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_ToUser = new System.Windows.Forms.Button();
            this.button_ToPackage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_ToUser
            // 
            this.button_ToUser.Location = new System.Drawing.Point(98, 99);
            this.button_ToUser.Name = "button_ToUser";
            this.button_ToUser.Size = new System.Drawing.Size(141, 71);
            this.button_ToUser.TabIndex = 0;
            this.button_ToUser.Text = "Zarządzaj urzytkownikami";
            this.button_ToUser.UseVisualStyleBackColor = true;
            this.button_ToUser.Click += new System.EventHandler(this.button_ToUser_Click);
            // 
            // button_ToPackage
            // 
            this.button_ToPackage.Location = new System.Drawing.Point(98, 215);
            this.button_ToPackage.Name = "button_ToPackage";
            this.button_ToPackage.Size = new System.Drawing.Size(141, 71);
            this.button_ToPackage.TabIndex = 1;
            this.button_ToPackage.Text = "Zarządzaj paczkami";
            this.button_ToPackage.UseVisualStyleBackColor = true;
            this.button_ToPackage.Click += new System.EventHandler(this.button_ToPackage_Click);
            // 
            // FormChose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(359, 464);
            this.Controls.Add(this.button_ToPackage);
            this.Controls.Add(this.button_ToUser);
            this.Name = "FormChose";
            this.Text = "FormChose";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_ToUser;
        private System.Windows.Forms.Button button_ToPackage;
    }
}