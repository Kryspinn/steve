﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steve
{
    public partial class FormLogin : Form
    {
        readonly FormPackageStatus _formPackageStatus = new FormPackageStatus();
        private bool _login = false;

        public delegate void PassedLoginAdminEventHandler(object o, EventArgs e);

        public event PassedLoginAdminEventHandler PassedLoginAdmin;

        protected virtual void OnPassedLoginAdmin()
        {
            PassedLoginAdmin?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnClosed(EventArgs e)
        {
            if (_login == false)
            {
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                base.OnClosed(e);
            }
        }

        public FormLogin()
        {
            InitializeComponent();
        }

        private void button_login_Click(object sender, EventArgs e)
        {

            if (textBox_login.Text == "Kryspin" && textBox_password.Text == "krystian1")
            {
                _login = true;
                OnPassedLoginAdmin();
            }
            else
            {
                MessageBox.Show("Błędny login lub hasło");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _formPackageStatus.ShowDialog();
        }
    }
}
