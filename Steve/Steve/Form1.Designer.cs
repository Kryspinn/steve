﻿namespace Steve
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_dodaj = new System.Windows.Forms.Button();
            this.btn_edytuj = new System.Windows.Forms.Button();
            this.btn_usun = new System.Windows.Forms.Button();
            this.listView_klient = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_dodaj
            // 
            this.btn_dodaj.Location = new System.Drawing.Point(45, 20);
            this.btn_dodaj.Margin = new System.Windows.Forms.Padding(2);
            this.btn_dodaj.Name = "btn_dodaj";
            this.btn_dodaj.Size = new System.Drawing.Size(83, 43);
            this.btn_dodaj.TabIndex = 0;
            this.btn_dodaj.Text = "Dodaj";
            this.btn_dodaj.UseVisualStyleBackColor = true;
            this.btn_dodaj.Click += new System.EventHandler(this.btn_dodaj_Click);
            // 
            // btn_edytuj
            // 
            this.btn_edytuj.Location = new System.Drawing.Point(173, 20);
            this.btn_edytuj.Margin = new System.Windows.Forms.Padding(2);
            this.btn_edytuj.Name = "btn_edytuj";
            this.btn_edytuj.Size = new System.Drawing.Size(83, 43);
            this.btn_edytuj.TabIndex = 1;
            this.btn_edytuj.Text = "Edytuj";
            this.btn_edytuj.UseVisualStyleBackColor = true;
            this.btn_edytuj.Click += new System.EventHandler(this.btn_edytuj_Click);
            // 
            // btn_usun
            // 
            this.btn_usun.Location = new System.Drawing.Point(304, 20);
            this.btn_usun.Margin = new System.Windows.Forms.Padding(2);
            this.btn_usun.Name = "btn_usun";
            this.btn_usun.Size = new System.Drawing.Size(83, 43);
            this.btn_usun.TabIndex = 2;
            this.btn_usun.Text = "Usun";
            this.btn_usun.UseVisualStyleBackColor = true;
            this.btn_usun.Click += new System.EventHandler(this.btn_usun_Click);
            // 
            // listView_klient
            // 
            this.listView_klient.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView_klient.FullRowSelect = true;
            this.listView_klient.GridLines = true;
            this.listView_klient.Location = new System.Drawing.Point(45, 94);
            this.listView_klient.Name = "listView_klient";
            this.listView_klient.Size = new System.Drawing.Size(342, 191);
            this.listView_klient.TabIndex = 6;
            this.listView_klient.UseCompatibleStateImageBehavior = false;
            this.listView_klient.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Imię";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Nazwisko";
            this.columnHeader2.Width = 97;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Adres";
            this.columnHeader3.Width = 174;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(394, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 297);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView_klient);
            this.Controls.Add(this.btn_usun);
            this.Controls.Add(this.btn_edytuj);
            this.Controls.Add(this.btn_dodaj);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_dodaj;
        private System.Windows.Forms.Button btn_edytuj;
        private System.Windows.Forms.Button btn_usun;
        public System.Windows.Forms.ListView listView_klient;
        public System.Windows.Forms.ColumnHeader columnHeader1;
        public System.Windows.Forms.ColumnHeader columnHeader2;
        public System.Windows.Forms.ColumnHeader columnHeader3;
        public System.Windows.Forms.Button button1;
    }
}

