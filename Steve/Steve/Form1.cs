﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Steve
{
    public partial class Form1 : Form
    {
        readonly FormAdd _fa = new FormAdd();
        readonly FormDelete _fd = new FormDelete();
        readonly FormEdit _fe = new FormEdit();
        readonly FormLogin _fl = new FormLogin();
        private bool _login = false;

        public Form1()
        {
            
            _fl.PassedLoginAdmin += PassedLoginAdmin;
            _fa.AddedNewRecord += OnAddedNewRecord;
            _fd.AddedNewRecord += OnAddedNewRecord;
            _fe.AddedNewRecord += OnAddedNewRecord;
            InitializeComponent();
            ChangeList();
        }

        //protected override void OnCreateControl()
        //{
        //    this.Hide();
        //    _fl.ShowDialog();
        //    base.OnCreateControl();
        //}

        protected override void OnActivated(EventArgs e)
        {
            ChangeList();
            //if (_login)
            //{
            //    ChangeList();
            //    base.OnActivated(e);
            //}
            //else
            //{
            //    this.Hide();
            //    _fl.ShowDialog();
            //}

        }

        private void btn_dodaj_Click(object sender, EventArgs e)
        {
           _fa.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            ChangeList();
        }

        public void OnAddedNewRecord(object o, EventArgs e)
        {
            ChangeList();
        }

        public void PassedLoginAdmin(object o, EventArgs e)
        {
            _login = true;
            this.Show();
            _fl.Close();
        }

        public void ChangeList ()
        {
            var listk = new List<Klient>();
            var xmlser = new XmlSerializer(typeof(List<Klient>));
            FileStream filestrem = new FileStream("E:\\KRD\\Steve\\Steve\\Klient.xml", FileMode.Open, FileAccess.Read);
            listk = (List<Klient>)xmlser.Deserialize(filestrem);
            
            filestrem.Close();
            listView_klient.Items.Clear();
            foreach (var kk in listk)
            {
                
                var item = new ListViewItem(kk.Name);
                item.SubItems.Add(kk.Surname);
                item.SubItems.Add(kk.Adress);
                listView_klient.Items.Add(item);
            }            
        }

        private void btn_usun_Click(object sender, EventArgs e)
        {
            _fd.ShowDialog();
        }

        private void btn_edytuj_Click(object sender, EventArgs e)
        {
            _fe.ShowDialog();
        }
    }
}
