﻿namespace Steve
{
    partial class FormAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_name = new System.Windows.Forms.TextBox();
            this.text_surname = new System.Windows.Forms.TextBox();
            this.text_adress = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.chosen_type = new System.Windows.Forms.ComboBox();
            this.zatwierdz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // text_name
            // 
            this.text_name.Location = new System.Drawing.Point(73, 69);
            this.text_name.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_name.Name = "text_name";
            this.text_name.Size = new System.Drawing.Size(177, 22);
            this.text_name.TabIndex = 0;
            this.text_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_surname
            // 
            this.text_surname.Location = new System.Drawing.Point(73, 118);
            this.text_surname.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_surname.Name = "text_surname";
            this.text_surname.Size = new System.Drawing.Size(177, 22);
            this.text_surname.TabIndex = 1;
            this.text_surname.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // text_adress
            // 
            this.text_adress.Location = new System.Drawing.Point(73, 170);
            this.text_adress.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.text_adress.Name = "text_adress";
            this.text_adress.Size = new System.Drawing.Size(177, 22);
            this.text_adress.TabIndex = 2;
            this.text_adress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(73, 210);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBox1.Size = new System.Drawing.Size(97, 21);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Regulamin";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // chosen_type
            // 
            this.chosen_type.FormattingEnabled = true;
            this.chosen_type.Items.AddRange(new object[] {
            "a",
            "b",
            "c"});
            this.chosen_type.Location = new System.Drawing.Point(73, 250);
            this.chosen_type.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chosen_type.Name = "chosen_type";
            this.chosen_type.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chosen_type.Size = new System.Drawing.Size(177, 24);
            this.chosen_type.TabIndex = 4;
            this.chosen_type.Text = "Rodzaj";
            // 
            // zatwierdz
            // 
            this.zatwierdz.Location = new System.Drawing.Point(73, 313);
            this.zatwierdz.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.zatwierdz.Name = "zatwierdz";
            this.zatwierdz.Size = new System.Drawing.Size(177, 43);
            this.zatwierdz.TabIndex = 5;
            this.zatwierdz.Text = "Zadwierdz";
            this.zatwierdz.UseVisualStyleBackColor = true;
            this.zatwierdz.Click += new System.EventHandler(this.zatwierdz_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 50);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Imię";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(73, 96);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nazwisko";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 151);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Adres";
            // 
            // FormAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 447);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.zatwierdz);
            this.Controls.Add(this.chosen_type);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.text_adress);
            this.Controls.Add(this.text_surname);
            this.Controls.Add(this.text_name);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FormAdd";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "Dodaj użytkownika";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_name;
        private System.Windows.Forms.TextBox text_surname;
        private System.Windows.Forms.TextBox text_adress;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox chosen_type;
        private System.Windows.Forms.Button zatwierdz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}