﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steve
{
    public partial class FormChose : Form
    {
        readonly Form1 _form1 = new Form1();
        readonly FormPackage _formPackage = new FormPackage();
        readonly FormLogin _formLogin = new FormLogin();
        private bool _login = false;


        public FormChose()
        {
            _formLogin.PassedLoginAdmin += PassedLoginAdmin;
            InitializeComponent();
        }
        protected override void OnCreateControl()
        {
            //this.Hide();
            _formLogin.ShowDialog();
            base.OnCreateControl();
        }
        protected override void OnActivated(EventArgs e)
        {
            if (_login)
            {
                
                base.OnActivated(e);
            }
            else
            {
                //this.Hide();
                _formLogin.ShowDialog();
            }

        }

        public void PassedLoginAdmin(object o, EventArgs e)
        {
            _login = true;
            this.Show();
            _formLogin.Close();
        }

        private void button_ToUser_Click(object sender, EventArgs e)
        {
            _form1.ShowDialog();
        }

        private void button_ToPackage_Click(object sender, EventArgs e)
        {
            _formPackage.ShowDialog();
        }
    }
}
