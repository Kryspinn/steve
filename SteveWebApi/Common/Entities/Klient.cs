﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Klient : BaseEntity
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Adress { get; set; }

    }
}
