﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
