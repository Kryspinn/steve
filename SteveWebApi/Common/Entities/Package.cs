﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Entities
{
    public class Package : BaseEntity
    {
        public string Status { get; set; }

        public string Date { get; set; }

        public int KlientId { get; set; }
    }
}
