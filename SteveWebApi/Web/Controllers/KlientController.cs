﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Common.Entities;
using Infrastructure.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("Steve/[controller]")]
    public class KlientController : Controller
    {
        private readonly IKlientService _klientService;

        public KlientController(IKlientService klientService)
        {
            _klientService = klientService;
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Post([FromBody]Klient klient)
        {
            _klientService.Create(klient);
            return Ok();
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult All()
        {
            var result = _klientService.GetKlients();
            return Json(result);
        }

        [HttpDelete]
        [Route("action")]
        public IActionResult Delete([FromBody]Klient klient)
        {
            _klientService.Delete(klient);
            return Ok();
        }
    }
}
