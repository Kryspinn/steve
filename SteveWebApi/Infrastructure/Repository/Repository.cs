﻿using Common.Entities;
using Infrastructure.DataAccess;
using Infrastructure.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly SteveContext steveContext;

        public Repository(SteveContext steveContext)
        {
            if (steveContext == null)
            {
                throw new ArgumentNullException("Context cant be empty");
            }

            this.steveContext = steveContext;
            steveContext.Database.EnsureCreated();
        }

        public void Create(T entity)
        {
            steveContext.Set<T>().Add(entity);
            steveContext.SaveChanges();
        }

        public IList<T> GetAll()
        {
            return steveContext.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return steveContext.Set<T>().FirstOrDefault(b => b.Id == id);
        }

        public void Remove(T entity)
        {
            steveContext.Set<T>().Remove(entity);
            steveContext.SaveChanges();
        }
    }
}
