﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repository.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Create(T entity);

        void Remove(T entity);

        T GetById(int id);

        IList<T> GetAll();
    }
}
