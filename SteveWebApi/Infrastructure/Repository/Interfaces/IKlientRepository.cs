﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repository.Interfaces
{
    public interface IKlientRepository : IRepository<Klient>
    {
        
    }
}
