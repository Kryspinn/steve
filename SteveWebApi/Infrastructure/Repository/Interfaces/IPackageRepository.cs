﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Repository.Interfaces
{
    public interface IPackageRepository : IRepository<Package>
    {
        IList<Package> GetAllToday();
    }
}
