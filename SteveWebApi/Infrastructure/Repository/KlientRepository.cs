﻿using Common.Entities;
using Infrastructure.DataAccess;
using Infrastructure.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repository
{
    public class KlientRepository : Repository<Klient>, IKlientRepository
    {
        public KlientRepository(SteveContext steveContext) : base(steveContext)
        {
        }
    }
}
