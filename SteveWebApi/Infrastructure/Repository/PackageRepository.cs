﻿using Common.Entities;
using Infrastructure.DataAccess;
using Infrastructure.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Repository
{
    public class PackageRepository : Repository<Package>, IPackageRepository
    {
        public PackageRepository(SteveContext steveContext) : base(steveContext)
        {
        }

        public IList<Package> GetAllToday()
        {
            return steveContext.Packages.Where(x => x.Date.GetHashCode() == DateTime.Today.GetHashCode()).ToList();
        }

    }
}
