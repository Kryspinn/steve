﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Services.Interface
{
    public interface IPackageService
    {
        IList<Package> GetPackages();

        void Create(Package package);

        void Delete(Package package);
    }
}
