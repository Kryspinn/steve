﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Services.Interface
{
    public interface IKlientService
    {
        IList<Klient> GetKlients();

        void Delete(Klient klient);

        void Create(Klient klient);
    }
}
