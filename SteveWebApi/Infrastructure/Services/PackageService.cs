﻿using Common.Entities;
using Infrastructure.Repository.Interfaces;
using Infrastructure.Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Services
{
    public class PackageService : IPackageService
    {
        private readonly IPackageRepository packageRepository;

        public PackageService(IPackageRepository packageRepository)
        {
            if(packageRepository == null)
            {
                throw new ArgumentNullException("Repo cant be empty");
            }
            this.packageRepository = packageRepository;
        }

        public void Create(Package package)
        {
            if (package == null)
            {
                throw new ArgumentNullException("package cant be empty");
            }
            packageRepository.Create(package);
        }

        public void Delete(Package package)
        {
            packageRepository.Remove(package);
        }

        public IList<Package> GetPackages()
        {
            return packageRepository.GetAll();
        }
    }
}
