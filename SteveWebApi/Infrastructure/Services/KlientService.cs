﻿using Common.Entities;
using Infrastructure.Repository.Interfaces;
using Infrastructure.Services.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Services
{
    public class KlientService : IKlientService
    {
        private readonly IKlientRepository klientRepository;

        public KlientService(IKlientRepository klientRepository)
        {
            if(klientRepository == null)
            {
                throw new ArgumentNullException("Repo cant be empty");
            }
            this.klientRepository = klientRepository;
        }

        public void Create(Klient klient)
        {
            klientRepository.Create(klient);
        }

        public void Delete(Klient klient)
        {
            klientRepository.Remove(klient);
        }

        public IList<Klient> GetKlients()
        {
            return klientRepository.GetAll();
        }
    }
}
