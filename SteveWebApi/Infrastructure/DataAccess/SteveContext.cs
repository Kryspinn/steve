﻿using Common.Entities;
using Infrastructure.Configurations;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructure.DataAccess
{
    public class SteveContext : DbContext
    {
        public SteveContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Klient> Klients { get; set; }
        public DbSet<Package> Packages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new KlientConfiguration());
            modelBuilder.ApplyConfiguration(new PackageConfiguration());
        }

    }
}
